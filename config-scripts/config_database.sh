#!/bin/sh

echo ##### INSTALL MYSQL #####

apt-get update



cat << EOF | debconf-set-selections
mysql-server-5.0 mysql-server/root_password password 123Senha
mysql-server-5.0 mysql-server/root_password_again password 123Senha
mysql-server-5.0 mysql-server/root_password seen true
mysql-server-5.0 mysql-server/root_password_again seen true
EOF

apt-get install -y mysql-server

sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf

service mysql restart

mysql -u root --password=123Senha -t <<STOP
-- Initial configuration of database
create database test_plugin;
show databases;
grant all privileges on test_plugin.* to 'appuser'@'%' IDENTIFIED BY '123Senha' WITH GRANT OPTION;
\q
STOP
test $? = 0 && echo "Your batch job terminated gracefully"

echo "create"
mysql -u appuser --password=123Senha -D test_plugin < /vagrant/config-scripts/config-files/estados.sql
mysql -u appuser --password=123Senha -D test_plugin < /vagrant/config-scripts/config-files/cidades.sql

echo Fim!
