package net.eldiosantos.maven.plugin.executor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlExecutor {

	private final Connection con;

	public SqlExecutor(Connection con) {
		super();
		this.con = con;
	}

	public ResultSet execute(final String sql) throws SQLException {
		final Statement st = con.createStatement();
		final ResultSet rs = st.executeQuery(sql);

		return rs;
	}
}
