package net.eldiosantos.maven.plugin.generator;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.eldiosantos.maven.plugin.parser.BlobDataParser;
import net.eldiosantos.maven.plugin.parser.BooleanDataParser;
import net.eldiosantos.maven.plugin.parser.ClobDataParser;
import net.eldiosantos.maven.plugin.parser.DataParser;
import net.eldiosantos.maven.plugin.parser.NumberDataParser;
import net.eldiosantos.maven.plugin.parser.StringDataParser;
import net.eldiosantos.maven.plugin.parser.TimestampDataParser;

public class GenerateDataString {

	private final List<DataParser>parserList = Arrays.asList(
			new StringDataParser()
			, new TimestampDataParser()
			, new ClobDataParser()
			, new BlobDataParser()
			, new BooleanDataParser()
			, new NumberDataParser()
		);

	public List<String>generate(final ResultSet rs) throws SQLException {
		final List<String>dataList = new ArrayList<String>();
		final ResultSetMetaData metaData = rs.getMetaData();
		while (rs.next()) {
			final StringBuffer data = new StringBuffer();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				final Object object = rs.getObject(i);
				data.append(getParser(object).parse(object));
				if(i < metaData.getColumnCount()) {
					data.append(", ");
				}
			}
			dataList.add(data.toString());
		}
		return dataList;
	}

	private DataParser getParser(Object data) {
		for(DataParser dp:parserList) {
			if(dp.carParse(data)) {
				return dp;
			}
		}

		return new DataParser() {

			public String parse(Object data) {
				return data!=null?data.toString():"null";
			}

			public Boolean carParse(Object data) {
				return true;
			}
		};
	}
}
