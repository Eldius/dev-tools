package net.eldiosantos.maven.plugin.generator;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class GenerateFieldList {

	public String generate(final ResultSetMetaData metaData) throws SQLException {
		final StringBuffer fields = new StringBuffer();
		for (int i = 1; i <= metaData.getColumnCount(); i++) {
			fields.append(metaData.getColumnName(i));
			if(i<metaData.getColumnCount()) {
				fields.append(", ");
			}
		}

		return fields.toString();
	}
}
