package net.eldiosantos.maven.plugin.generator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ScriptGenerator {

	public String generate(final ResultSet rs, final String table) throws SQLException {
		final StringBuffer script = new StringBuffer();
		final GenerateDataString generateDataString = new GenerateDataString();
		final String fieldList = new GenerateFieldList().generate(rs.getMetaData());

		final List<String> dataStringList = generateDataString.generate(rs);
		for(String data:dataStringList) {
			final String scriptLine = new StringBuffer("insert into ")
				.append(table)
				.append("(")
				.append(fieldList)
				.append(") values (")
				.append(data)
				.append(");\n").toString();
			script.append(scriptLine);
		}
		return  script.toString();
	}
}
