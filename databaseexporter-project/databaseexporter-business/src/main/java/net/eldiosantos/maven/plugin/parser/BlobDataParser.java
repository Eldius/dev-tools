package net.eldiosantos.maven.plugin.parser;

import java.sql.Blob;

import org.apache.commons.io.IOUtils;

public class BlobDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return (data instanceof Blob);
	}

	public String parse(Object data) {
		try {
			final Blob blob = (Blob)data;
			final String string = IOUtils.toString(blob.getBinaryStream());
			return new StringBuffer("'").append(string).append("'").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
