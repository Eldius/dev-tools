package net.eldiosantos.maven.plugin.parser;

public class BooleanDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return data instanceof Boolean;
	}

	public String parse(Object data) {
		return ((Boolean)data)?"1":"0";
	}

}
