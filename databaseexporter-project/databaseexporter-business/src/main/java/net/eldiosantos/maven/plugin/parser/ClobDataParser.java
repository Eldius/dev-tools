package net.eldiosantos.maven.plugin.parser;

import java.sql.Clob;

import org.apache.commons.io.IOUtils;

public class ClobDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return (data instanceof Clob);
	}

	public String parse(Object data) {
		try {
			final Clob clob = (Clob)data;

			final String string = IOUtils.toString(clob.getAsciiStream());

			return new StringBuffer("'").append(string).append("'").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
