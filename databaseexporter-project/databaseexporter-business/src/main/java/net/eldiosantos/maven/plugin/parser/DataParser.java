package net.eldiosantos.maven.plugin.parser;

public interface DataParser {

	public abstract Boolean carParse(Object data);
	public abstract String parse(Object data);
}
