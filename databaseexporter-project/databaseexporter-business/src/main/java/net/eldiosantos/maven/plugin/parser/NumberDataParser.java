package net.eldiosantos.maven.plugin.parser;

public class NumberDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return data instanceof Number;
	}

	public String parse(Object data) {
		return data.toString();
	}

}
