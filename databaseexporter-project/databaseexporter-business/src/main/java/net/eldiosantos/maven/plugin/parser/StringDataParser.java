package net.eldiosantos.maven.plugin.parser;

public class StringDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return (data instanceof String);
	}

	public String parse(Object data) {
		return new StringBuffer("'").append(data.toString().replaceAll("'", "''")).append("'").toString();
	}

}
