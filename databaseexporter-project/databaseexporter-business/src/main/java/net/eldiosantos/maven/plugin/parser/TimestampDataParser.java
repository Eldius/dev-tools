package net.eldiosantos.maven.plugin.parser;

import java.sql.Timestamp;

public class TimestampDataParser implements DataParser {

	public Boolean carParse(Object data) {
		return (data instanceof Timestamp);
	}

	public String parse(Object data) {
		return new StringBuffer("'").append(data.toString()).append("'").toString();
	}

}
