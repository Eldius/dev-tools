package net.eldiosantos.maven.plugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import net.eldiosantos.maven.plugin.executor.SqlExecutor;

import net.eldiosantos.maven.plugin.generator.ScriptGenerator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Goal to extract data from database to SQL script file with inserts.
 *
 */
@Mojo(name = "export")
public class ExportData extends AbstractMojo {
	/**
	 * Location of the file.
	 * 
	 * @parameter property="${outputDirectory}"
	 * @required
	 */
	@Parameter(name = "outputFile", defaultValue = "${project.build.directory}/script.sql")
	private File outputFile;

	/**
	 * Database URL.
	 * 
	 * @parameter property="${url}"
	 * @required
	 */
	@Parameter(name = "url")
	private String url;

	/**
	 * Database user.
	 * 
	 * @parameter property="${user}"
	 * @required
	 */
	@Parameter(name = "user")
	private String user;

	/**
	 * Database pass.
	 * 
	 * @parameter property="${pass}"
	 * @required
	 */
	@Parameter(name = "pass")
	private String pass;

	/**
	 * Database driverClass.
	 * 
	 * @parameter property="${driverClass}"
	 * @required
	 */
	@Parameter(name = "driverClass")
	private String driverClass;

	/**
	 * List of tables to export.
	 * 
	 * @parameter property="${tables}"
	 * @required
	 */
	@Parameter(name = "tables")
	private List<String> tables;


	public void execute() throws MojoExecutionException {
		PrintWriter writer = null;
		try {

			if(outputFile.exists()) {
				outputFile.delete();
			}

			new File(outputFile.getParent()).mkdirs();
			outputFile.createNewFile();

			writer = new PrintWriter(outputFile);
			Connection con = getConnection();

			for (String table : tables) {
				final String sql = "select * from " + table;
				final ResultSet rs = new SqlExecutor(con).execute(sql);
				final String generatedScript = new ScriptGenerator().generate(rs, table);

                if(getLog().isDebugEnabled()){
                    getLog().debug(generatedScript);
                }

				writer.append(generatedScript);
				writer.flush();
			}

		} catch (Exception e) {
			e.printStackTrace();
            throw new MojoExecutionException("Error trying to export database data.", e);
		} finally {
			getLog().info("Closing file...");
			if(writer != null){
				writer.flush();
				writer.close();
			}
		}
	}

	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driverClass);
		return DriverManager.getConnection(url, user, pass);
	}
}
