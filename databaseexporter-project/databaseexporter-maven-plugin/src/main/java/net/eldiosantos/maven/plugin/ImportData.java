package net.eldiosantos.maven.plugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Goal to extract data from database to SQL script file with inserts.
 *
 */
@Mojo(name = "import")
public class ImportData extends AbstractMojo {
	/**
	 * Location of the file.
	 * 
	 * @parameter property="${outputDirectory}"
	 * @required
	 */
	@Parameter(name = "inputFile", defaultValue = "${project.build.directory}/script.sql")
	private File inputFile;

	/**
	 * Database URL.
	 * 
	 * @parameter property="${testUrl}"
	 * @required
	 */
	@Parameter(name = "testUrl")
	private String url;

	/**
	 * Database user.
	 * 
	 * @parameter property="${testUser}"
	 * @required
	 */
	@Parameter(name = "testUser")
	private String user;

	/**
	 * Database pass.
	 * 
	 * @parameter property="${testPass}"
	 * @required
	 */
	@Parameter(name = "testPass")
	private String pass;

	/**
	 * Database driverClass.
	 * 
	 * @parameter property="${testDriverClass}"
	 * @required
	 */
	@Parameter(name = "testDriverClass")
	private String driverClass;

	public void execute() throws MojoExecutionException {
		
	}

	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driverClass);
		return DriverManager.getConnection(url, user, pass);
	}
}
