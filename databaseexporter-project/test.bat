cls

set PROJECT_ROOT=%CD%


cd %PROJECT_ROOT%
call mvn clean install

cd %PROJECT_ROOT%\test-project
call mvn databaseexporter:export

cd %PROJECT_ROOT%
