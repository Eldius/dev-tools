package net.eldiosantos.tools.frontendtools;

import net.eldiosantos.tools.frontendtools.service.NodebinariesDownloader;

/**
 * Created by esjunior on 02/05/2016.
 */
public class App {
    public static void main(String[] params) throws Exception {
        //new NodebinariesDownloader().download("v5.9.1", "target/node_bin");
        new NodebinariesDownloader().download("v0.8.19", "target/node_bin");
    }
}
