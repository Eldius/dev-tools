package net.eldiosantos.tools.frontendtools.model;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class VersionDescriptor {

    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("files")
    @Expose
    private List<String> files = new ArrayList<String>();
    @SerializedName("npm")
    @Expose
    private String npm;
    @SerializedName("v8")
    @Expose
    private String v8;
    @SerializedName("uv")
    @Expose
    private String uv;
    @SerializedName("zlib")
    @Expose
    private String zlib;
    @SerializedName("openssl")
    @Expose
    private String openssl;
    @SerializedName("modules")
    @Expose
    private String modules;
    @SerializedName("lts")
    @Expose
    private Boolean lts;

    /**
     *
     * @return
     * The version
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     * The version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The files
     */
    public List<String> getFiles() {
        return files;
    }

    /**
     *
     * @param files
     * The files
     */
    public void setFiles(List<String> files) {
        this.files = files;
    }

    /**
     *
     * @return
     * The npm
     */
    public String getNpm() {
        return npm;
    }

    /**
     *
     * @param npm
     * The npm
     */
    public void setNpm(String npm) {
        this.npm = npm;
    }

    /**
     *
     * @return
     * The v8
     */
    public String getV8() {
        return v8;
    }

    /**
     *
     * @param v8
     * The v8
     */
    public void setV8(String v8) {
        this.v8 = v8;
    }

    /**
     *
     * @return
     * The uv
     */
    public String getUv() {
        return uv;
    }

    /**
     *
     * @param uv
     * The uv
     */
    public void setUv(String uv) {
        this.uv = uv;
    }

    /**
     *
     * @return
     * The zlib
     */
    public String getZlib() {
        return zlib;
    }

    /**
     *
     * @param zlib
     * The zlib
     */
    public void setZlib(String zlib) {
        this.zlib = zlib;
    }

    /**
     *
     * @return
     * The openssl
     */
    public String getOpenssl() {
        return openssl;
    }

    /**
     *
     * @param openssl
     * The openssl
     */
    public void setOpenssl(String openssl) {
        this.openssl = openssl;
    }

    /**
     *
     * @return
     * The modules
     */
    public String getModules() {
        return modules;
    }

    /**
     *
     * @param modules
     * The modules
     */
    public void setModules(String modules) {
        this.modules = modules;
    }

    /**
     *
     * @return
     * The lts
     */
    public Boolean getLts() {
        return lts;
    }

    /**
     *
     * @param lts
     * The lts
     */
    public void setLts(Boolean lts) {
        this.lts = lts;
    }

}