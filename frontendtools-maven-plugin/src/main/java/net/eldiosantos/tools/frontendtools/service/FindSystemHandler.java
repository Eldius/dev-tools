package net.eldiosantos.tools.frontendtools.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by esjunior on 02/05/2016.
 */
public class FindSystemHandler {
    public Optional<OSDescriptor> verify() {

        final Map<String, String> env = new HashMap<>(System.getenv());
        System.getProperties().entrySet().forEach(e->env.put(e.getKey().toString(), e.getValue().toString()));

        return Arrays.asList(OSDescriptor.values()).parallelStream()
            .filter(desc -> desc.canHandleThisOS(env))
            .findAny();
    }
}
