package net.eldiosantos.tools.frontendtools.service;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by esjunior on 02/05/2016.
 */
public class NodebinariesDownloader {
    public void download(final String ver, final String destPath) throws Exception {

        new VersionsVerifier().get().parallelStream()
                .filter(vd -> vd.getVersion().equalsIgnoreCase(ver))
                .peek(v -> System.out.println(String.format("Returning version: %s", v.getVersion())))
                .findAny()
                .ifPresent(vd -> {
                    new FindSystemHandler().verify()
                            .ifPresent(desc -> {
                                final URL nodeDownloadLink = desc.getNodeDownloadLink(vd);
                                System.out.println(nodeDownloadLink);

                                try(final InputStream in = (InputStream) nodeDownloadLink.openConnection().getContent()) {

                                    final File destFolder = new File(destPath);
                                    System.out.println("Preparing destination folder...");
                                    if(destFolder.exists()) {
                                        destFolder.delete();
                                    }
                                    destFolder.mkdirs();
                                    System.out.println("Downloading node binary...");
                                    System.out.println(String.format("downlaoding from '%s'", nodeDownloadLink));
                                    Files.copy(in, Paths.get(destPath, "node.exe"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                });

    }
}
