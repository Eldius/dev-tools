package net.eldiosantos.tools.frontendtools.service;

import net.eldiosantos.tools.frontendtools.model.VersionDescriptor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by esjunior on 02/05/2016.
 */
public enum OSDescriptor {
    WINDOWS_X64("win-x64") {
        public Boolean canHandleThisOS(final Map<String, String> env) {
            return env.entrySet().parallelStream()
                    //.peek(e-> System.out.println(String.format("%s => %s", e.getKey(), e.getValue())))
                    .filter(e->e.getKey().equalsIgnoreCase("OS") && e.getValue().toUpperCase().contains("WINDOWS"))
                    .filter(e->e.getKey().equalsIgnoreCase("sun.arch.data.model") && e.getValue().equalsIgnoreCase("64"))
                    .count() > 0;
        }
    }
    , WINDOWS_X86("win-x86") {
        public Boolean canHandleThisOS(final Map<String, String> env) {
            return env.entrySet().parallelStream()
                    //.peek(e-> System.out.println(String.format("%s => %s", e.getKey(), e.getValue())))
                    .filter(e->e.getKey().equalsIgnoreCase("OS") && e.getValue().toUpperCase().contains("WINDOWS"))
                    .filter(e->e.getKey().equalsIgnoreCase("sun.arch.data.model") && e.getValue().equalsIgnoreCase("32"))
                    .count() > 0;
        }
    };
    private static final String basePath = "https://nodejs.org/dist/";
    private final String binFilePath;

    OSDescriptor(String binFilePath) {
        this.binFilePath = binFilePath;
    }

    public abstract Boolean canHandleThisOS(Map<String, String> env);

    public URL getNodeDownloadLink(final VersionDescriptor descriptor) {
        try {
            return new URL(
                    new StringBuffer(basePath)
                            .append(descriptor.getVersion())
                            .append("/")
                            .append(binFilePath)
                            .append("/")
                            .append("node.exe")
                            .toString()
            );
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Error creating the file download link...");
        }
    }
}
