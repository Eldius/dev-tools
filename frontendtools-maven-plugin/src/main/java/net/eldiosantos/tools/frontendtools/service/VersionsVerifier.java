package net.eldiosantos.tools.frontendtools.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.eldiosantos.tools.frontendtools.model.VersionDescriptor;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 02/05/2016.
 */
public class VersionsVerifier {

    public static final String DESCRIPTOR_URL = "https://nodejs.org/dist/index.json";

    public List<VersionDescriptor> get() throws Exception {
        List<VersionDescriptor> versionsDescriptors;
        try(final InputStream in = (InputStream) new URL(DESCRIPTOR_URL).openConnection().getContent()) {

            final String json = new BufferedReader(new InputStreamReader(in))
                    .lines()
                    .collect(Collectors.joining());
            Type listType = new TypeToken<ArrayList<VersionDescriptor>>() {}.getType();
            versionsDescriptors = new Gson().fromJson(json, listType);
        } catch (Exception e) {
            versionsDescriptors = null;
        }

        return versionsDescriptors;
    }
}
