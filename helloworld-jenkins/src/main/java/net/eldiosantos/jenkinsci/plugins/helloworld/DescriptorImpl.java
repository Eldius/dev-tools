package net.eldiosantos.jenkinsci.plugins.helloworld;

import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import hudson.util.FormValidation;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Created by Eldius on 27/12/2014.
 */
@Extension
public class DescriptorImpl extends BuildStepDescriptor<Builder> {
    /**
     * To persist global configuration information,
     * simply store it in a field and call save().
     *
     * <p>
     * If you don't want fields to be persisted, use <tt>transient</tt>.
     */
    private boolean active;

    private String token;

    /**
     * In order to load the persisted global configuration, you have to
     * call load() in the constructor.
     */
    public DescriptorImpl() {
        super(HelloWorldBuilder.class);
        load();
    }

    /**
     * Performs on-the-fly validation of the form field 'name'.
     *
     * @param value
     *      This parameter receives the value that the user has typed.
     * @return
     *      Indicates the outcome of the validation. This is sent to the browser.
     *      <p>
     *      Note that returning {@link hudson.util.FormValidation#error(String)} does not
     *      prevent the form from being saved. It just means that a message
     *      will be displayed to the user.
     */
    public FormValidation doCheckName(@QueryParameter String value)
            throws IOException, ServletException {
        if (value.length() == 0)
            return FormValidation.error("Please set a name");
        if (value.length() < 4)
            return FormValidation.warning("Isn't the name too short?");
        return FormValidation.ok();
    }

    public boolean isApplicable(Class<? extends AbstractProject> aClass) {
        // Indicates that this builder can be used with all kinds of project types
        return true;
    }

    /**
     * This human readable name is used in the configuration screen.
     */
    public String getDisplayName() {
        return "Say hello world";
    }

    @Override
    public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
        // To persist global configuration information,
        // set that to properties and call save().
        System.out.println("active: " + formData.get("active").toString());
        active = formData.getBoolean("active");
        token = formData.getString("token");
        // ^Can also use req.bindJSON(this, formData);
        //  (easier when there are many fields; need set* methods for this, like setUseFrench)
        save();
        return super.configure(req,formData);
    }

    /**
     * This method returns true if the global configuration says we should speak French.
     *
     * The method name is bit awkward because global.jelly calls this method to determine
     * the initial state of the checkbox by the naming convention.
     */
    public boolean getActive() {
        return active;
    }

    public boolean isActive() {
        return active;
    }

    public String getToken() {
        return token;
    }
}

