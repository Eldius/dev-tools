package net.eldiosantos.jenkinsci.plugins.helloworld.model;

/**
 * Created by Eldius on 27/12/2014.
 */
public class BuildMarker {

    private String id;
    private String mark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
