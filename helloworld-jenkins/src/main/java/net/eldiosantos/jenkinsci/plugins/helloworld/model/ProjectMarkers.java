package net.eldiosantos.jenkinsci.plugins.helloworld.model;

import java.util.List;

/**
 * Created by Eldius on 27/12/2014.
 */
public class ProjectMarkers {

    private List<BuildMarker>markers;

    public List<BuildMarker> getMarkers() {
        return markers;
    }

    public void setMarkers(List<BuildMarker> markers) {
        this.markers = markers;
    }
}
