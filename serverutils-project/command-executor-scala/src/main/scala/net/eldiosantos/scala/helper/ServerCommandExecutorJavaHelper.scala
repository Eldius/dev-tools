package net.eldiosantos.scala.helper

import net.eldiosantos.scala.process.executor.SshExecutor
import net.eldiosantos.scala.process.executor.command.CommandFactory
import net.eldiosantos.scala.process.executor.connection.{HostDefinition, UserDefinition}

/**
  * Created by eldio.junior on 25/02/2015.
  */
class ServerCommandExecutorJavaHelper {
   def execute(userDefinition: UserDefinition, hostDefinition: HostDefinition, command: String): String = {
       new SshExecutor(hostDefinition, userDefinition).execute(CommandFactory.exec(command))
   }
 }
