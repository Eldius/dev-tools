package net.eldiosantos.scala.helper

import net.eldiosantos.scala.process.executor.{SftpExecutor, ResourceDefinition, ResourceDefinitionFactory, SshExecutor}
import net.eldiosantos.scala.process.executor.command.CommandFactory
import net.eldiosantos.scala.process.executor.connection.{HostDefinition, UserDefinition}

/**
   * Created by eldio.junior on 25/02/2015.
   */
class ServerFileCopyExecutorJavaHelper {
    def execute(user: UserDefinition, host: HostDefinition, origin: ResourceDefinition, destination: ResourceDefinition): String = {

      val command = CommandFactory.sftp().
        setOrig(origin).
        setDest(destination).
        build()

      new SftpExecutor(host, user).execute(command)
    }
  }
