package net.eldiosantos.scala.process

import java.io.{ByteArrayOutputStream, OutputStream}

import com.jcraft.jsch._
import net.eldiosantos.scala.process.executor.command.CommandFactory
import net.eldiosantos.scala.process.executor.connection.{UserDefinition, HostDefinition}
import net.eldiosantos.scala.process.executor.{ResourceDefinitionFactory, connection, SftpExecutor, SshExecutor}

/**
 * Created by Eldius on 04/01/2015.
 */
object MainApp extends scala.App{

  val host = "localhost"
  val port = 2222
  val user = "vagrant"
  val pass = "vagrant"

  sendFile
  executeCommand

  def executeCommand: Any = {

    val command = CommandFactory.exec(" uname -a ")

    val executor = new SshExecutor(new HostDefinition("localhost", 2222), new UserDefinition("vagrant", "vagrant"))

    println(executor.execute(command))
  }

  def sendFile: Any = {
    val executor = new SftpExecutor(new HostDefinition("localhost", 2222), new UserDefinition("vagrant", "vagrant"))
    val command = CommandFactory.sftp().setOrig(ResourceDefinitionFactory.local("pom.xml")).
      setDest(ResourceDefinitionFactory.remote("/vagrant/serverutils-project/command-executor-scala/target")).
      build()
    executor.execute(command)
  }

}
