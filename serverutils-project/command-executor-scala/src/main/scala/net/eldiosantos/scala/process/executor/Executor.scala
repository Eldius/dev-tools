package net.eldiosantos.scala.process.executor

import com.jcraft.jsch.{ChannelSftp, Channel, JSch, Session}
import net.eldiosantos.scala.process.executor.command.Command
import net.eldiosantos.scala.process.executor.connection.{HostDefinition, UserDefinition}

/**
 * Created by Eldius on 04/01/2015.
 */
trait Executor {

  val user: UserDefinition
  val host: HostDefinition
  val jSch: JSch

  def getSession(): Session = {
    val session = jSch.getSession(user.getUser(), host.getHost(), host.getPort())
    session.setPassword(user.getPass())
    session.setConfig("StrictHostKeyChecking", "no");

    session
  }

  def execute(command: Command): String

  protected def getChannel(session: Session, channelType: String): Channel = {
    session.openChannel(channelType)
  }

  protected def exec(command: Command, action: (Channel, Session) => String): String = {
    val session = getSession()
    session.connect()

    val channel = getChannel(session, command.commandType())

    val response = action(channel, session)

    if(channel.isConnected) {
      channel.disconnect()
    }
    if(session.isConnected) {
      session.disconnect()
    }

    response
  }
}
