package net.eldiosantos.scala.process.executor

/**
 * Created by Eldius on 04/01/2015.
 */
trait ResourceDefinition {
  def path(): String
  def resourceType: String
  def resource(): String = resourceType + ":" + path()
}

class RemoteResource(resourcePath: String) extends ResourceDefinition {

  override def path(): String = resourcePath
  override def resourceType: String = "remote"
}

class LocalResource(resourcePath: String) extends ResourceDefinition {
  override def resource(): String = "local:" + resourcePath
  override def path(): String = resourcePath

  override def resourceType: String = "local"
}

object ResourceDefinitionFactory {
  def local(resourcePath: String): ResourceDefinition = {
    new LocalResource(resourcePath)
  }

  def remote(resourcePath: String): ResourceDefinition = {
    new RemoteResource(resourcePath)
  }
}
