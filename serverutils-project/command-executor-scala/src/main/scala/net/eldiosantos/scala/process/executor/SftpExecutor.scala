package net.eldiosantos.scala.process.executor

import com.jcraft.jsch._
import net.eldiosantos.scala.process.executor.command.{SftpCommand, Command}
import net.eldiosantos.scala.process.executor.command.CommandFactory.SftpFactoryResourceBuilder
import net.eldiosantos.scala.process.executor.connection.{UserDefinition, HostDefinition}

/**
 * Created by Eldius on 04/01/2015.
 */
class SftpExecutor (hostDef: HostDefinition, userDef: UserDefinition, jSch1: JSch = new JSch) extends Executor {

  val user = userDef
  val host = hostDef
  val jSch = jSch1

  override def execute(command: Command): String = {
    def action(channel: Channel, session: Session): String = {
      val ch = channel.asInstanceOf[ChannelSftp]
      ch.connect(3000)

      val cm = command.asInstanceOf[SftpCommand]

      if(cm.getOrig.resourceType == "local") {
        ch.put(cm.getOrig.path(), cm.getDest.path())
      } else if(cm.getOrig.resourceType == "remote") {
        ch.get(cm.getOrig.path(), cm.getDest.path())
      } else {
        return "Wrong usage"
      }

      "Finished"
    }

    super.exec(command, action)
  }
}

