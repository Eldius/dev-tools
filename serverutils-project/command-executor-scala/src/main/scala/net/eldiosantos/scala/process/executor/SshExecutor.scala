package net.eldiosantos.scala.process.executor

import com.jcraft.jsch._
import net.eldiosantos.scala.process.executor.command.{ExecCommand, Command}
import net.eldiosantos.scala.process.executor.connection.{UserDefinition, HostDefinition}

/**
 * Created by Eldius on 04/01/2015.
 */
class SshExecutor (hostDef: HostDefinition, userDef: UserDefinition, jSch1: JSch = new JSch) extends Executor {

  val user = userDef
  val host = hostDef
  val jSch = jSch1

  override def execute(command: Command): String = {
    def action(channel: Channel, session: Session): String = {
      val ch = channel.asInstanceOf[ChannelExec]
      ch.setCommand(command.asInstanceOf[ExecCommand].getCommand())
      ch.connect()

      val in = ch.getInputStream
      val response = scala.io.Source.fromInputStream(in).getLines().mkString("\n")

      response
    }

    super.exec(command, action)
  }
}
