package net.eldiosantos.scala.process.executor.command

import net.eldiosantos.scala.process.executor.{ResourceDefinitionFactory, ResourceDefinition}

/**
 * Created by Eldius on 04/01/2015.
 */
trait Command {
  def commandType(): String
  def resource:Any
}

class ExecCommand(command:String) extends Command{
  override def commandType(): String = "exec"

  override def resource: Any = command

  def getCommand() = command
}

class SftpCommand(from: ResourceDefinition, to: ResourceDefinition) extends Command {
  override def commandType(): String = "sftp"

  override def resource: Any = {
    Map("orig" -> from, "dest" -> to)
  }

  def getOrig: ResourceDefinition = {
    from
  }

  def getDest: ResourceDefinition = {
    to
  }
}

object CommandFactory{
  def exec(command: String) = new ExecCommand(command)
  def sftp() = new SftpFactoryResourceBuilder

  class SftpFactoryResourceBuilder() {
    var orig: ResourceDefinition = null
    var dest: ResourceDefinition = null

    def setOrig(res: ResourceDefinition): SftpFactoryResourceBuilder = {
      orig = res
      this
    }

    def setDest(res: ResourceDefinition): SftpFactoryResourceBuilder = {
      dest = res
      this
    }

    def validate(): Boolean = {
      (orig!=null) && (dest!=null)
    }

    def build(): Command = {
      if(validate()) {
        return new SftpCommand(getOrig(), getDest())
      } else {
        throw new IllegalArgumentException("Invalid arguments")
      }
    }

    def getOrig() = orig
    def getDest() = dest
  }
}

