package net.eldiosantos.scala.process.executor.connection

/**
 * Created by Eldius on 04/01/2015.
 */
class HostDefinition(host: String, port: Int) {

  def getHost() = host
  def getPort() = port
}
