package net.eldiosantos.scala.process.executor.connection

/**
 * Created by Eldius on 04/01/2015.
 */
class UserDefinition(user: String, pass: String) {

  def getUser() = user
  def getPass() = pass
}
