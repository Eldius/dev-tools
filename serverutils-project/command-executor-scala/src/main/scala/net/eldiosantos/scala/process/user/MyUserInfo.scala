package net.eldiosantos.scala.process.user

import com.jcraft.jsch.UserInfo

/**
 * Created by Eldius on 04/01/2015.
 */
class MyUserInfo (val password: String) extends UserInfo {
  override def getPassphrase: String = password

  override def promptPassword(message: String): Boolean = false

  override def promptYesNo(message: String): Boolean = false

  override def showMessage(message: String): Unit = println(message)

  override def getPassword: String = password

  override def promptPassphrase(message: String): Boolean = false
}
