
import net.eldiosantos.scala.process.executor.SshExecutor

val host = "localhost"

val port = 2222

val user = "vagrant"
val pass = "vagrant"

val executor = new SshExecutor(host, port, user, pass)

println (executor.execute("cd /vagrant && ls -la"))
