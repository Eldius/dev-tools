package net.eldiosantos.tools.beans;

import net.eldiosantos.scala.process.executor.ResourceDefinition;
import net.eldiosantos.scala.process.executor.ResourceDefinitionFactory;

/**
 * Created by eldio.junior on 26/02/2015.
 */

public class ResourceHandler {
    public static enum ResourceType {
        REMOTE()
        , LOCAL();
    }

    private ResourceType type;
    private String resourcePath;

    public void setType(ResourceType type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = ResourceType.valueOf(type.toUpperCase());
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public ResourceType getType() {
        return type;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public ResourceDefinition buildResource() {

        switch (this.getType()) {
            case LOCAL:
                return ResourceDefinitionFactory.local(this.getResourcePath());
            case REMOTE:
                return ResourceDefinitionFactory.remote(this.getResourcePath());
            default:
                throw new IllegalArgumentException("Illegal resource type.");
        }

    }
}
