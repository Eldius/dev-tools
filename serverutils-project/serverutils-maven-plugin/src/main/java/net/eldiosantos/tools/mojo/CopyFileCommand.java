package net.eldiosantos.tools.mojo;

import net.eldiosantos.scala.helper.ServerFileCopyExecutorJavaHelper;
import net.eldiosantos.scala.process.executor.ResourceDefinition;
import net.eldiosantos.scala.process.executor.connection.HostDefinition;
import net.eldiosantos.scala.process.executor.connection.UserDefinition;
import net.eldiosantos.tools.beans.ResourceHandler;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Copy a resource origin the origin destination the destination.
 */
@Mojo(
        name = "copy"
        , defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST
        , threadSafe = false
)
public class CopyFileCommand extends AbstractMojo {

    /**
     * Resource destination be copied.
     */
    @Parameter(
            name = "origin"
            , required = true
    )
    private ResourceHandler origin;


    /**
     * Where resource needs destination be copied.
     */
    @Parameter(
            name = "destination"
            , required = true
    )
    private ResourceHandler destination;

    /**
     * Remote host identification (network name or IP address).
     */
    @Parameter(
            name = "host"
            , defaultValue = "localhost"
    )
    private String host;

    /**
     * SSH port number of the remote host.
     */
    @Parameter(
            name = "port"
            , defaultValue = "2222"
    )
    private Integer port;

    /**
     * Username destination log in into the remote host.
     */
    @Parameter(
            name = "username"
            , defaultValue = "vagrant"
    )
    private String username;

    /**
     * Password destination log in into the remote host.
     */
    @Parameter(
            name = "password"
            , defaultValue = "vagrant"
    )
    private String password;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        getLog().info("----------------------------------------------------------------");
        getLog().info("\tStarting execution...");

        try {
            final ResourceDefinition fromResource = this.origin.buildResource();
            final ResourceDefinition toResource = this.destination.buildResource();
            getLog().info(String.format("About to copy resource from '%s' to '%s'", fromResource.resource(), toResource.resource()));

            final UserDefinition userDefinition = new UserDefinition(this.username, this.password);

            final HostDefinition hostDef = new HostDefinition(this.host, this.port);

            final String executionResult = new ServerFileCopyExecutorJavaHelper().execute(userDefinition, hostDef, fromResource, toResource);

            getLog().info("Command executed successfully.");

            getLog().info("#################### SERVER RESULT ####################");
            getLog().info(executionResult);
            getLog().info("#################### SERVER RESULT ####################");
        } catch (Exception e) {
            throw new MojoExecutionException("Error executing plugin.", e);
        } finally {
            getLog().info("\tFinishing execution...");
            getLog().info("----------------------------------------------------------------");
        }
    }
}
