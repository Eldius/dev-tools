package net.eldiosantos.tools.mojo;

import net.eldiosantos.scala.helper.ServerCommandExecutorJavaHelper;
import net.eldiosantos.scala.process.executor.connection.HostDefinition;
import net.eldiosantos.scala.process.executor.connection.UserDefinition;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Executes a command or a script on the remote
 * machine by SSH protocol.
 * Created by eldio.junior on 25/02/2015.
 */
@Mojo(
        name = "execute"
        , defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST
        , threadSafe = true
)
public class ExecuteServerCommand extends AbstractMojo {

    /**
     * Script file to execute on the remote machine.
     * If this property is null it will execute the
     * command in the command attribute.
     */
    @Parameter(
            name = "scriptFile"
    )
    private File scriptFile;

    /**
     * Command to execute on  the remote machine.
     * It will only be executed if scriptFile is null.
     */
    @Parameter(
            name = "command"
            , defaultValue = "uname -a"
    )
    private String command;

    /**
     * Remote host identification (network name or IP address).
     */
    @Parameter(
            name = "host"
            , defaultValue = "localhost"
    )
    private String host;

    /**
     * SSH port number of the remote host.
     */
    @Parameter(
            name = "port"
            , defaultValue = "2222"
    )
    private Integer port;

    /**
     * Username to log in into the remote host.
     */
    @Parameter(
            name = "username"
            , defaultValue = "vagrant"
    )
    private String username;

    /**
     * Password to log in into the remote host.
     */
    @Parameter(
            name = "password"
            , defaultValue = "vagrant"
    )
    private String password;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        getLog().info("----------------------------------------------------------------");
        getLog().info("\tStarting execution...");

        try {
            getLog().info(String.format("About to execute command '%s' at '%s'", this.command, this.host));

            UserDefinition userDefinition = new UserDefinition(this.username, this.password);

            HostDefinition hostDef = new HostDefinition(this.host, this.port);

            String executionResult = new ServerCommandExecutorJavaHelper().execute(userDefinition, hostDef, this.scriptFile != null ? readFile() : this.command);

            getLog().info("Command executed successfully.");

            getLog().info("#################### SERVER RESULT ####################");
            getLog().info(executionResult);
            getLog().info("#################### SERVER RESULT ####################");
        } catch (Exception e) {
            throw new MojoExecutionException("Error executing plugin.", e);
        } finally {
            getLog().info("\tFinishing execution...");
            getLog().info("----------------------------------------------------------------");
        }
    }

    private String readFile() throws FileNotFoundException {
        final Scanner sc = new Scanner(this.scriptFile);
        final StringBuffer content = new StringBuffer();

        while(sc.hasNextLine()) {
            content.append(sc.nextLine())
                    .append("\n");
        }

        return content.toString();
    }
}
